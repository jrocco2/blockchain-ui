import axios from 'axios';

import './CustomerView.css';
import React, { Component } from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import OrderTable from '../OrderTable/OrderTable';
import InstoHeader from "../InstoHeader/InstoHeader";

class Default extends Component {

	constructor(props) {
		super(props);
		this.state = {
			activeTab: '1',
			hasLoaded: false,
			no_records: 4
		};
		this.toggle = this.toggle.bind(this);
		let userId = props.location.search.split('=')[1] || 2;
		let endPoint = 'https://us-central1-global-ace-213402.cloudfunctions.net/blockchain-queries';
		axios.all([
			axios.post(endPoint, {
				query: 'SELECT * FROM frontend.users WHERE user_id = ' + userId
			}),
			axios.post(endPoint, {
				query: 'SELECT * from frontend.orders WHERE user_id = ' + userId + ' ORDER BY order_date DESC LIMIT 4'
			})])
			.then(axios.spread((userDetails, orderDetails) => {
				this.setState(
					{
						userDetails: userDetails.data[0],
						orderDetails: orderDetails.data,
						hasLoaded: true
					});
			}));
	}

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}

	render() {
		const options = ["Fulton Group", "McDonalds", "ANZ", "Burger King", "KFC", "Mixer Inc.", "NAB", "Pro Processing", "Nandos", "CBA"];
		return (this.state.hasLoaded &&
            <InstoHeader>
			<div className="container">
				<h3 className={"customer-header"}>Welcome {this.state.userDetails['name']}</h3>
                <div className="row row-form-width">
                        <form>
                            <div className="row row-form-width">
                                <div className="form-group col s6">
									<label htmlFor="supplier">Supplier</label>
                                    <Typeahead
										disabled={true}
										labelKey="name"
										multiple={false}
										options={options}
										placeholder={"Fulton Group"}
									/>

                                </div>
								<div className="form-group col s6">
									<label htmlFor="order_title">Order Title</label>
                                    <input
										id="order_title"
										type="text"
										className="form-control"
									/>

                                </div>
                            </div>

							<div className="row row-form-width">
								<div className="form-group col s6">
									<label htmlFor="price">Price</label>
                                    <input
										id="price"
										type="text"
										className="form-control"
									/>

                                </div>
                                <div className="form-group col s6">
									<label htmlFor="delivery_date">Delivery Date</label>
                                    <input
										id="delivery_date"
										type="text"
										className="form-control"
									/>

                                </div>
                            </div>

                            <div className="row row-form-width">
                                <div className="form-group col s12">
									<label htmlFor="textarea1">Order Description</label>
                                    <textarea
										id="textarea1"
										className="materialize-textarea"
									> </textarea>
                                </div>
                            </div>
                            <button className="btn waves-effect waves-light float-right" type="submit" name="action">Submit
                                <i className="material-icons right">send</i>
                            </button>
							</form>
						</div>
                <h3>Order History</h3>
                <OrderTable
					orderDetails={this.state.orderDetails}
					userDetails={this.state.userDetails}
				/>
            </div>
            </InstoHeader>


		);

	}
}

export default Default;