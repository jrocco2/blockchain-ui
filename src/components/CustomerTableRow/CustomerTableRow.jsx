import './CustomerTableRow.css';

import React from 'react';
import { Modal } from 'react-materialize'
import { Typeahead } from 'react-bootstrap-typeahead';

class OrderTableCell extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			isDisabled: true
		};

		this.toggle = this.toggle.bind(this);
		this.toggleEdit = this.toggleEdit.bind(this);

	}

	toggle() {
		this.setState({
			modal: !this.state.modal,
			isDisabled: true
		});
	}

	toggleEdit() {
		this.setState({
			isDisabled: !this.state.isDisabled
		});
	}

	render() {
		let statuses = {
			1: ["Pending", "text-white bg-amber"],
			2: ["Accepted", "text-white bg-blue"],
			3: ["Delivered", "text-white bg-success"],
			4: ["Declined", "text-white bg-danger"]
		};
		const options = ["Fulton Group", "McDonalds", "ANZ", "Burger King", "KFC", "Mixer Inc.", "NAB", "Pro Processing", "Nandos", "CBA"];
		const { orderid, supplier_name, order_date, delivery_date, status, price, order_name, order_description, image } = this.props.order;
		const statusStyle = statuses[status][1];
		return (
			<React.Fragment>
				<tr onClick={this.toggle}>
					<td>{orderid}</td>
					<td>
						<img src={image} alt={"supplier"} className={'circle small-circle'}></img>
					</td>
					<td>{new Date(order_date).toLocaleString().split(',')[0]}</td>
					<td>{new Date(delivery_date).toLocaleString().split(',')[0]}</td>
					<td className="status-cell text-right">
						<div className={` badge text-uppercase ${statusStyle}`}>{statuses[status][0]}</div>
					</td>
					<td className="text-right">
						<span className="icon-btn size-30">
							<i className="zmdi zmdi-more-vert zmdi-hc-lg" /></span>
					</td>
				</tr>

				<Modal
				  header={

				  <div className={"row"}>

						  <div className={"form-group col-sm-6"}>
							  {'Purchase Order: ' + orderid}
						  </div>

						  <div className={"col-sm-6"}>

							  <div className={"row switch-row"}>
								  <div className={"col-sm-9"}></div>
								  <div className={"col-sm-3 switch-column"}>
								  	<label htmlFor="edit_content">Edit Order Details</label>
								  </div>
							  </div>

							  <div className={"row"}>
								  <div className={"col-sm-9"}></div>
									<div className="col-sm-3 switch">
										<label>
											Off
											<input onClick={this.toggleEdit} type="checkbox" checked={this.state.isDisabled}/>
												<span className="lever"></span>
												On
										</label>
									</div>
						      </div>

						  </div>
				  </div>}
				  fixedFooter
				  open={this.state.modal}
				  ref={node => { this.node = node; }}
				  >

                    <div className="row row-form-width">
                        <form>
                            <div className="row row-form-width">
                                <div className="form-group col s6">
									<label htmlFor="supplier">Supplier</label>
                                    <Typeahead
										disabled={this.state.isDisabled}
										labelKey="name"
										multiple={false}
										options={options}
										placeholder={supplier_name}
									/>

                                </div>
								<div className="form-group col s6">
									<label htmlFor="order_title">Order Title</label>
                                    <input
										disabled={this.state.isDisabled}
										id="order_title"
										type="text"
										className="form-control"
										defaultValue={order_name}
									/>

                                </div>
                            </div>

							<div className="row row-form-width">
								<div className="form-group col s6">
									<label htmlFor="price">Price</label>
                                    <input
										disabled={this.state.isDisabled}
										id="price"
										type="text"
										className="form-control"
										defaultValue={price}
									/>

                                </div>
                                <div className="form-group col s6">
									<label htmlFor="delivery_date">Delivery Date</label>
                                    <input
										disabled={this.state.isDisabled}
										id="delivery_date"
										type="text"
										className="form-control"
										defaultValue={new Date(delivery_date).toLocaleString().split(',')[0]}
									/>

                                </div>
                            </div>

                            <div className="row row-form-width">
                                <div className="form-group col s12">
									<label htmlFor="textarea1">Order Description</label>
                                    <textarea
										disabled={this.state.isDisabled}
										id="textarea1"
										className="materialize-textarea"
										defaultValue={order_description}
									></textarea>
                                </div>
                            </div>

							<div className={"row row-form-width"}>
									<div className={"col s4"}>
										<label htmlFor="supplier">Packer</label>
										<Typeahead
											disabled={this.state.isDisabled}
											labelKey="name"
											multiple={false}
											options={options}
											placeholder={supplier_name}
										/>
									</div>
							</div>
							<div className="row row-form-width">
								<div className="form-group col s10">
									<label htmlFor="textarea1">Order Description</label>
									<textarea
										disabled={this.state.isDisabled}
										id="textarea1"
										className="materialize-textarea"
										defaultValue={order_description}
									></textarea>
								</div>
								<div className={"col s2"}>
									<a className="btn-floating btn-large waves-effect waves-light blue float-right"><i
										className="material-icons">add</i></a>
								</div>
							</div>

                        </form>
                    </div>

				</Modal>
			</React.Fragment>

		);
	}
}

export default OrderTableCell;
