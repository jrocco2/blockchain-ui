import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import SupplierView from '../SupplierView/SupplierView';
import ProcessorView from '../ProcessorView/ProcessorView';
import CustomerView from '../CustomerView/CustomerView';
import Login from '../BlockchainLogin/BlockchainLogin';

class App extends React.Component {

	render() {
		return (
			<BrowserRouter>
			<Switch>
				<Route path='/mixer' component={CustomerView} />
				<Route exact path='/' component={Login} />
				<Route path='/supplier' component={SupplierView} />
				<Route path='/packer' component={ProcessorView} />
			</Switch>
			</BrowserRouter>
		);
	}
}
export default App;