import React, { Component } from 'react';
import CustomerTableRow from '../CustomerTableRow/CustomerTableRow';


class OrderTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			orders: this.props.orderDetails,
			isSupplier: this.props.userDetails["is_supplier"]

		};
	}

	render() {
		return (
			<div>
                <table className={"highlight"}>
					<thead>
					  <tr>
						  <th>OrderId</th>
						  <th>Supplier</th>
						  <th>Order Date</th>
						  <th>Delivery Date</th>
						  <th className={"text-right"}>Status</th>
					  </tr>
					</thead>

					<tbody>
						{this.state.orders.map((order, iter) => {
								return (
										<CustomerTableRow
											key={iter}
											order={order}
											isSupplier={this.state.isSupplier}
										/>
								);
							})}
					</tbody>
				  </table>
			</div>
		);
	}
}

export default OrderTable;