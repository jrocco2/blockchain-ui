import React from 'react';
import "./anz-brand.css";
import './InstoHeader.css';


class InstoHeader extends React.Component {

	constructor(props) {
		super(props);

	}


    render() {

        return (
            <div className="InstoHeader">
                <div className="insta-header insta-bar">
                    {this.props.children}

                    <div className="logo">
                        <a href="/" title="ANZ Analytics" id="logo"></a>
                    </div>
				</div>

\

				<div className="clearfix"></div>
			</div>

        );
    }
}

export default InstoHeader;