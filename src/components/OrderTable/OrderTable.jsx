import React, { Component } from 'react';
import OrderTableCell from '../OrderTableCell/OrderTableCell';

import {Collapsible, CollapsibleItem} from 'react-materialize'

class OrderTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			orders: this.props.orderDetails,
			subcontracts: this.props.subcontractDetails,
			isSupplier: this.props.userDetails["is_supplier"],
			isMixer: this.props.userDetails["is_customer"]

		};
	}

	render() {
		return (
			<div>
				<Collapsible popout defaultActiveKey={2}>
				  <CollapsibleItem header={

					<div className={"row collapse-row"}>
					  <div className={"col s3"}>
						  <b>Order Id</b>
					  </div>
						<div className={"col s3"}>
							<b>Order Date</b>
					  </div>
						<div className={"col s3"}>
							<b>Delivery Date</b>
					  </div>
						<div className={"col s3 row-align"}>
							<b>Status</b>
					  </div>

					</div>

				  }>
				  </CollapsibleItem>
					{this.state.orders.map((order, iter) => {
								let subcontract_details = [];

						{this.state.isSupplier && this.state.subcontracts.forEach(contract => {
									if (order['orderid'] === contract["orderid"] ) {
										subcontract_details.push(contract)
									}
								});}
								return (
										<OrderTableCell
											key={iter}
											counter={iter}
											order={order}
											isSupplier={this.state.isSupplier}
											isMixer={this.state.isMixer}
											subcontracts = {subcontract_details}
										/>
								);
							})}
				</Collapsible>
			</div>
		);
	}
}

export default OrderTable;