import axios from 'axios';

import './SupplierView.css';

import React, { Component } from 'react';
import OrderTable from '../OrderTable/OrderTable';
import InstoHeader from "../InstoHeader/InstoHeader";

class Default extends Component {

	constructor(props) {
		super(props);
		this.state = {
			activeTab: '1',
			hasLoaded: false,
			no_records: 4
		};
		this.toggle = this.toggle.bind(this);
		let userId = props.location.search.split('=')[1] || 1;
		let endPoint = 'https://us-central1-global-ace-213402.cloudfunctions.net/blockchain-queries';
		axios.all([
			axios.post(endPoint, {
				query: 'SELECT * FROM frontend.users WHERE user_id = ' + userId
			}),
			axios.post(endPoint, {
				query: 'SELECT * FROM frontend.subcontracts WHERE sub_id IN (SELECT sub_id FROM frontend.order_subcontract WHERE order_id IN (SELECT orderid FROM frontend.orders WHERE supplier_id = ' + userId + 'ORDER BY order_date DESC LIMIT 4))'
			}),
			axios.post(endPoint, {
				query: 'SELECT * from frontend.orders WHERE supplier_id = ' + userId + ' ORDER BY order_date DESC LIMIT 6'
			})])
			.then(axios.spread((userDetails, subcontractDetails, orderDetails) => {
				this.setState(
					{
						userDetails: userDetails.data[0],
						orderDetails: orderDetails.data,
						subcontractDetails: subcontractDetails.data,
						hasLoaded: true
					});
			}));
	}

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}

	render() {
		return (this.state.hasLoaded &&

			<InstoHeader>
			<div className="container">
				<h3 className={"customer-header"}>Welcome {this.state.userDetails['name']}</h3>
				<OrderTable
					orderDetails={this.state.orderDetails}
					userDetails={this.state.userDetails}
					subcontractDetails={this.state.subcontractDetails}
				/>
			</div>
			</InstoHeader>


		);

	}
}

export default Default;