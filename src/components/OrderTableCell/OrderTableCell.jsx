import './OrderTableCell.css';

import React from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import {CollapsibleItem} from 'react-materialize'
import PackerDetails from '../PackerDetails/PackerDetails';

class OrderTableCell extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			isDisabled: true,
			subcontracts: this.props.subcontracts
		};

		this.toggle = this.toggle.bind(this);
		this.createEmptyContract = this.createEmptyContract.bind(this);
		this.toggleEdit = this.toggleEdit.bind(this);

	}

	toggle() {
		this.setState({
			modal: !this.state.modal,
			isDisabled: true
		});
	}

	createEmptyContract() {
		let empty_contract = {order_description:"",
								orderid: "",
								packer_company:"",
								packer_id:"",
								price:"",
								sub_id:""};
		let old_contracts = this.state.subcontracts.concat(empty_contract);
		this.setState({
			subcontracts: old_contracts
		});

	}


	toggleEdit() {
		this.setState({
			isDisabled: !this.state.isDisabled
		});
	}

	render() {
		let statuses = {
			1: ["Pending", "text-white orange"],
			2: ["Accepted", "text-white blue"],
			3: ["Delivered", "text-white bg-success"],
			4: ["Declined", "text-white bg-danger"]
		};
		const options = ["Fulton Group", "McDonalds", "ANZ", "Burger King", "KFC", "Mixer Inc.", "NAB", "Pro Processing", "Nandos", "CBA"];
		const { orderid, supplier_name, order_date, delivery_date, status, price, order_name, order_description } = this.props.order;
		const statusStyle = statuses[status][1];
		return (
			<React.Fragment>
				<CollapsibleItem className={"accordion " + ((this.props.isSupplier && this.props.counter === 1) && "active")} header={

				  	<div className={"row collapse-row"}>
					  <div className={"col s2"}>
						  {orderid}
					  </div>
						<div className={"col s2"}>
							{new Date(order_date).toLocaleString().split(',')[0]}
					  </div>
						<div className={"col s2"}>
							{new Date(delivery_date).toLocaleString().split(',')[0]}
					  </div>
						<div className={"col s2 restrict-row"}>
							<div className={` badge text-uppercase ${statusStyle}`}>{statuses[status][0]}</div>
					  </div>
							<a className={"btn-floating green " +
							((statuses[status][0] !== "Pending" || this.props.isMixer) && "i-am-invisible") + " button-align"}>
								<i className="material-icons icon-center">check</i>
							</a>





					</div>

				  }>
					{/*BODY GOES HERE */}
					{
						<React.Fragment>
							<div className={"row"}>

								  <div className={"form-group col-sm-6"}>
									  <h5>{'Purchase Order: ' + orderid}</h5>
								  </div>

								  <div className={"col-sm-6"}>

									  <div className={"row switch-row"}>
										  <div className={"col-sm-9"}></div>
										  <div className={"col-sm-3 switch-column"}>
											<label htmlFor="edit_content">Edit</label>
										  </div>
									  </div>

									  <div className={"row"}>
										  <div className={"col-sm-9"}></div>
											<div className="col-sm-3 switch">
												<label>

													<input onClick={this.toggleEdit} type="checkbox" checked={!this.state.isDisabled}/>
														<span className="lever"></span>

												</label>
											</div>
									  </div>

								  </div>
							</div>

						<div className="row row-form-width">
                        <form>
                            <div className="row row-form-width">
                                <div className="form-group col s6">
									<label htmlFor="supplier">Supplier</label>
                                    <Typeahead
										disabled={true}
										labelKey="name"
										multiple={false}
										options={options}
										placeholder={supplier_name || "Fulton Group"}
									/>

                                </div>
								<div className="form-group col s6">
									<label htmlFor="order_title">Order Title</label>
                                    <input
										disabled={this.state.isDisabled}
										id="order_title"
										type="text"
										className="form-control"
										defaultValue={order_name}
									/>

                                </div>
                            </div>

							<div className="row row-form-width">
								<div className="form-group col s6">
									<label htmlFor="price">Price</label>
                                    <input
										disabled={this.state.isDisabled}
										id="price"
										type="text"
										className="form-control"
										defaultValue={price}
									/>

                                </div>
                                <div className="form-group col s6">
									<label htmlFor="delivery_date">Delivery Date</label>
                                    <input
										disabled={this.state.isDisabled}
										id="delivery_date"
										type="text"
										className="form-control"
										defaultValue={new Date(delivery_date).toLocaleString().split(',')[0]}
									/>

                                </div>
                            </div>

                            <div className="row row-form-width">
                                <div className="form-group col s12">
									<label htmlFor="textarea1">Order Description</label>
                                    <textarea
										disabled={this.state.isDisabled}
										id="textarea1"
										className="materialize-textarea"
										defaultValue={order_description}
									></textarea>
                                </div>
                            </div>

							{this.state.subcontracts.map((contract, iter) => {
									return(
										<PackerDetails
											key={iter}
											isDisabled={this.state.isDisabled}
											options={options}
											supplier_name={contract["packer_company"]}
											price={contract["price"]}
											order_description={contract["order_description"]}
									/>)
								})
							}

							{this.props.isSupplier && <div className={"center-align"}>
								<a onClick={this.createEmptyContract} className="btn-floating btn-large blue"><i
									className="material-icons">add</i></a>
							</div>}

							{!this.state.isDisabled &&
								<button className="btn waves-effect waves-light float-right blue" type="submit" name="action">Submit
                                	<i className="material-icons right">send</i>
                            	</button>
							}

							</form>
						</div>
					</React.Fragment>
					}
				  </CollapsibleItem>
			</React.Fragment>

		);
	}
}

export default OrderTableCell;
