import React from 'react';
//import ReactDOM from "react-dom";
//import { AppContainer } from "react-hot-loader";
import { HashRouter } from 'react-router-dom';

import Routes from "blockchain_scf/Components/Routes";


const renderPage = function (elem) {
	ReactDOM.render(
		<HashRouter>
			<Routes />
		</HashRouter>,
		elem
	);
};

export {
	//	Entry point functions for pages
	renderPage,

	// Components
	Routes
};

// "I drink your milkshake!"