import './PackerDetails.css';

import React from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';

class OrderTableCell extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		};

	}

	render() {
		return (
			<React.Fragment>
				<div className={"row row-form-width"}>
					<div className={"col s6"}>
						<label htmlFor="supplier">Packer</label>
						<Typeahead
							disabled={this.props.isDisabled}
							labelKey="name"
							multiple={false}
							options={this.props.options || ""}
							placeholder={this.props.supplier_name || ""}
						/>
					</div>
					<div className="form-group col s6">
						<label htmlFor="price">Price</label>
						<input
							disabled={this.props.isDisabled}
							id="price"
							type="text"
							className="form-control"
							value={this.props.price || ""}
						/>

					</div>
					<div className="form-group col s12">
						<label htmlFor="textarea1">Order Description</label>
						<textarea
							disabled={this.props.isDisabled}
							id="textarea1"
							className="materialize-textarea"
							value={this.props.order_description || ""}
						></textarea>
					</div>

			</div>
			</React.Fragment>

		);
	}
}

export default OrderTableCell;
