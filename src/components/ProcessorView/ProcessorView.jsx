import axios from 'axios';

import './ProcessorView.css';
import React, { Component } from 'react';
import InstoHeader from "../InstoHeader/InstoHeader";
import OrderTable from '../OrderTable/OrderTable';
class ProcessorView extends Component {

	constructor(props) {
		super(props);
		this.state = {
			activeTab: '1',
			hasLoaded: false,
			no_records: 4
		};
		this.toggle = this.toggle.bind(this);
		let userId = props.location.search.split('=')[1] || 3;
		let endPoint = 'https://us-central1-global-ace-213402.cloudfunctions.net/blockchain-queries';
		axios.all([
			axios.post(endPoint, {
				query: 'SELECT * FROM frontend.users WHERE user_id = ' + userId
			}),
			axios.post(endPoint, {
				query: 'SELECT * FROM frontend.subcontracts WHERE packer_id IN (SELECT company_id FROM frontend.users WHERE user_id = ' + userId + ')'
			})])
			.then(axios.spread((userDetails, subcontractDetails) => {
				this.setState(
					{
						userDetails: userDetails.data[0],
						subcontractDetails: subcontractDetails.data,
						hasLoaded: true
					});
			}));
	}

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}

	render() {
		const options = ["Fulton Group", "McDonalds", "ANZ", "Burger King", "KFC", "Mixer Inc.", "NAB", "Pro Processing", "Nandos", "CBA"];
		return (this.state.hasLoaded &&
			<InstoHeader>
			<div className="container">
				<h3 className={"customer-header"}>Welcome {this.state.userDetails['name']}</h3>
                <OrderTable
					orderDetails={this.state.subcontractDetails}
					userDetails={this.state.userDetails}
				/>
			</div>
			</InstoHeader>

		);

	}
}

export default ProcessorView;