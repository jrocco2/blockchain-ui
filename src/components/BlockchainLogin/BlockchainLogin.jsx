import React from 'react';

import './BlockchainLogin.css';
import InstoHeader from "../InstoHeader/InstoHeader";

class BlockchainLogin extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			
		}
	}

	componentDidMount() {

	}

    
	render() {
		return (
			<InstoHeader>
				<div className="container">
					<div className="row customer-header">
						<a href="/mixer"><button type="button" className="btn blue button-layout">Mixer</button></a>
					</div>
					<div className="row">
						<a href="/supplier"><button type="button" className="btn blue button-layout">Supplier</button></a>
					</div>
					<div className="row">
						<a href="/packer"><button type="button" className="btn blue button-layout">Packer</button></a>
					</div>
				</div>
			</InstoHeader>
		);
	}
}

export default BlockchainLogin;